const swcConfigs = {
  module: {
    type: "commonjs",
  },
};

module.exports = {
  transform: {
    "^.+\\.(t|j)sx?$": ["@swc/jest", swcConfigs],
  },
  testEnvironment: "jsdom",
  setupFilesAfterEnv: ["./jest-setup.ts"],
  moduleNameMapper: {
    '@mui/styled-engine': '<rootDir>/node_modules/@mui/styled-engine-sc',
    '^Components/(.*)$': '<rootDir>/src/components/$1',
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.ts"
  }
};

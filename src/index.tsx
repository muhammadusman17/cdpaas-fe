import React from 'react';
import ReactDOM from 'react-dom';
import App from 'Components/App/App';
import { Provider as StoreProvider } from 'react-redux';
import { store } from './store/store';
import { SWRConfig } from 'swr';
import fetcher from './utils/fetcher';
import GlobalStyle from './GlobalStyle';

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider store={store}>
      <GlobalStyle />
      <SWRConfig value={{ fetcher }}>
        <App />
      </SWRConfig>
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

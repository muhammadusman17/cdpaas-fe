import React from 'react';
import Navbar from './Navbar';
import { render } from '../../utils/test-utils';

test('Header contains correct text', async () => {
  const { getByText } = render(<Navbar />, {
    preloadedState: { user: { authenticated: true, userData: { username: 'John Doe' } } },
  });
  expect(getByText('Logout')).toBeInTheDocument();
});

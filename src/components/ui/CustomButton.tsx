import { styled } from '@mui/system';

const CustomButton = styled('button')({
  backgroundColor: 'white',
  color: 'palevioletred',
  fontSize: '1em',
  margin: '1em',
  width: '300px',
  height: '60px',
  border: '5px solid palevioletred',
  ':hover': {
    backgroundColor: '#e9e7e7',
    cursor: 'pointer',
  },
  ':active': {
    backgroundColor: 'gray',
    color: 'white',
  },
});

export default CustomButton;

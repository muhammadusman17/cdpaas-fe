/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Button, TextField } from '@mui/material';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const formSchema = yup.object().shape({
  exampleRequired: yup.string().required(),
  example: yup
    .number()
    .min(5, 'Must be more than 5')
    .nullable()
    .transform((value: string, originalValue: string) => (originalValue.trim() === '' ? null : value)),
});

const ExampleForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    resolver: yupResolver(formSchema),
    mode: 'onChange',
  });

  const onSubmit = (data) => window.console.log(data);

  return (
    <div
      style={{
        backgroundColor: 'rgba(255,255,255,0.9)',
        padding: '5px 20px',
        marginTop: 15,
        borderRadius: 10,
      }}
    >
      <h4
        style={{
          margin: '5px 0px 12px',
          color: 'black',
        }}
      >
        Form using react-hook-form and yup
      </h4>
      <form onSubmit={handleSubmit(onSubmit)}>
        <TextField
          id="outlined-error"
          error={!!errors.example}
          label="Age (number)"
          type="number"
          helperText={!!errors.example ? errors.example.message : ''}
          {
            ...register('example') /* eslint-disable-line */
          }
        />

        <TextField
          id="outlined-error"
          error={!!errors.exampleRequired}
          label="Name"
          helperText={!!errors.exampleRequired ? 'Name is required' : ''}
          {
            ...register('exampleRequired') /* eslint-disable-line */
          }
        />

        <Button style={{ margin: 5 }} type="submit" disabled={!isValid} variant="outlined">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default ExampleForm;

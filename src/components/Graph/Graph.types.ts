export interface NodeType {
  id: string;
  name: string;
  level: number;
  verticalPosition?: number;
  width?: number;
  height?: number;
  nodeImage?: string;
  configurationContent?: any;
  modalContent?: (configurationContent: any, onSubmit: (data: any) => void, onClose: () => void) => JSX.Element;
}
export interface LinkType {
  source: string;
  target: string;
}

export interface DataType {
  nodes: NodeType[];
  links: LinkType[];
}

export interface GraphProps {
  graphData: DataType;
  linkType: 'default' | 'blocked';
  nodeType: 'rectangle' | 'image';
  newNodeAdded?: 'config' | 'drag';
  setNewNodeAdded?: (status: 'config' | 'drag' | undefined) => void;
}

export interface Point {
  x: number;
  y: number;
}

import React from 'react';
import ForceGraph2D, { LinkObject, NodeObject } from 'react-force-graph-2d';
import { GraphProps, NodeType } from './Graph.types';
const Graph: React.FC<GraphProps> = ({ graphData, linkType, nodeType }) => {
  const [maxLevel, setMaxLevel] = React.useState(0);
  const [data, setData] = React.useState(graphData);
  const [araningNodes, setArangingNodes] = React.useState('Start');

  React.useEffect(() => {
    if (data.nodes)
      setMaxLevel(
        data.nodes.reduce((prevLevel, node) => {
          return prevLevel > node.level ? prevLevel : node.level;
        }, 0) + 1,
      );
  }, [data.nodes]);
  React.useEffect(() => {
    if (graphData) {
      setData(graphData);
      setArangingNodes('Start');
    }
  }, [graphData]);

  React.useEffect(() => {
    // this will auto arange the nodes
    if (araningNodes === 'Start' && maxLevel) {
      setArangingNodes('Inprogress');
      const nodesOnEachLevel = Array(maxLevel).fill(0);
      data.nodes.forEach((node) => {
        nodesOnEachLevel[node.level] += 1;
      });
      nodesOnEachLevel.forEach((totalNodes, level) => {
        let position = totalNodes % 2 === 0 ? -6 : 0;
        const updatedData = data.nodes.map((node) => {
          if (node.level === level) {
            node.verticalPosition = position;
            position = position < 0 ? position * -1 : position * -1 - 12;
          }
          return node;
        });
        setData({
          ...data,
          nodes: updatedData,
        });
      });
      setArangingNodes('Done');
    }
  }, [araningNodes, data, maxLevel]);

  //graph functions
  const customNodeObjects = React.useCallback(
    (node: NodeObject, ctx: CanvasRenderingContext2D) => {
      // for positioning the nodes
      if ((node as NodeType).level === -1 && maxLevel) {
        node.fx = maxLevel * 20;
        node.fy = maxLevel * -10;
      } else {
        node.fx = node.x;
        node.fy = (node as NodeType).verticalPosition;
      }

      if (nodeType === 'image' && (node as NodeType).nodeImage) {
        (node as NodeType).width = 10;
        (node as NodeType).height = 10;
        const imageObj1 = new Image();
        imageObj1.src = (node as NodeType).nodeImage as string;
        ctx.drawImage(
          imageObj1,
          (node.x as number) - ((node as NodeType).width as number) / 2,
          (node.y as number) - ((node as NodeType).height as number) / 2,
          (node as NodeType).width as number,
          (node as NodeType).height as number,
        );
      } else {
        const label = (node as NodeType).name;
        const fontSize = 3;
        ctx.font = `${fontSize}px Sans-Serif`;
        (node as NodeType).width = label.length < 6 ? 12 : label.length * 2;
        (node as NodeType).height = 8;
        ctx.fillStyle = 'black';
        ctx.fillRect(
          (node.x as number) - ((node as NodeType).width as number) / 2,
          (node.y as number) - ((node as NodeType).height as number) / 2,
          (node as NodeType).width as number,
          (node as NodeType).height as number,
        );

        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = 'white';
        ctx.fillText(label as string, node.x as number, node.y as number);
      }
    },
    [maxLevel, nodeType],
  );
  const customLinkObjects = React.useCallback(
    (link: LinkObject, ctx: CanvasRenderingContext2D) => {
      const source = link.source as NodeObject & NodeType;
      const target = link.target as NodeObject & NodeType;
      const targetNodeWidth = target.width as number;
      const sourceNodeWidth = source.width as number;
      ctx.strokeStyle = 'gray';
      ctx.lineWidth = 1;
      // draw a red line
      ctx.beginPath();
      ctx.moveTo((source.fx as number) + sourceNodeWidth / 2, source.fy as number);

      if (linkType === 'blocked') {
        ctx.lineTo(((source.fx as number) + (target.fx as number)) / 2, source.fy as number);
        ctx.lineTo(((source.fx as number) + (target.fx as number)) / 2, target.fy as number);
      }
      ctx.lineTo((target.fx as number) - targetNodeWidth / 2, target.fy as number);

      //creating arrow head of link

      if (linkType === 'blocked') {
        ctx.moveTo((target.fx as number) - targetNodeWidth / 2 - 2, target.fy as number);
        ctx.lineTo((target.fx as number) - targetNodeWidth / 2 - 2, (target.fy as number) - 1);
        ctx.lineTo((target.fx as number) - targetNodeWidth / 2, target.fy as number);
        ctx.lineTo((target.fx as number) - targetNodeWidth / 2 - 2, (target.fy as number) + 1);
        ctx.lineTo((target.fx as number) - targetNodeWidth / 2 - 2, target.fy as number);
      }
      ctx.stroke();
    },
    [linkType],
  );

  const fgRef = React.useRef();

  React.useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    if (maxLevel) (fgRef.current as any).zoom(11 - (maxLevel - 1));
  }, [maxLevel]);

  return (
    <ForceGraph2D
      // width={900}
      // height={370}
      graphData={data}
      ref={fgRef}
      linkWidth={5}
      dagMode="lr"
      dagLevelDistance={50}
      enableZoomInteraction={false}
      enableNodeDrag={false}
      d3AlphaMin={0.3}
      nodeCanvasObject={customNodeObjects}
      linkCanvasObject={customLinkObjects}
    />
  );
};

export default Graph;

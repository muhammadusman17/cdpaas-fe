import { NodeObject } from 'react-force-graph-2d';
import { NodeType, Point } from './Graph.types';

export const doOverlap = (l1: Point, r1: Point, l2: Point, r2: Point) => {
  if (l1.x >= r2.x || l2.x >= r1.x) return false;
  if (r1.y <= l2.y || r2.y <= l1.y) return false;
  return true;
};

export const getReactNodesCoordinates = (item: NodeObject & NodeType) => {
  const topX = (item.x as number) - (item.width as number) / 2;
  const topY = (item.y as number) - (item.height as number) / 2;
  const bottomX = (item.x as number) + (item.width as number) / 2;
  const bottomY = (item.y as number) + (item.height as number) / 2;
  const left: Point = { x: topX, y: topY };
  const right: Point = { x: bottomX, y: bottomY };
  return [left, right];
};

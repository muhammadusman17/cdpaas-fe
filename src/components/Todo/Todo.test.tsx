import React from 'react';
import Todo from './Todo';
import { render } from '../../utils/test-utils';
import { waitFor } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';

export const handlers = [
  rest.post('/login', null),
  // Handles a GET /user request
  rest.get('https://jsonplaceholder.typicode.com/todos/1', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        title: 'Dummy Title',
      }),
    );
  }),
];

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen();
});

afterAll(() => {
  server.close();
});

test('Todo Title Shown On Request Success', async () => {
  const { getByText } = render(<Todo />);
  await waitFor(() => expect(getByText('Dummy Title')).toBeInTheDocument());
});

import React from 'react';
import useSWR from 'swr';

const Todo = () => {
  const { data } = useSWR('https://jsonplaceholder.typicode.com/todos/1');
  if (!data) {
    return <p>Loading......</p>;
  }
  return <p style={{ color: 'white' }}>{data.title}</p>;
};

export default Todo;

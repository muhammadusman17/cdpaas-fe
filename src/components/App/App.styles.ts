import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  app: {
    height: '91.5vh',
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#282C34',
  },
  logo: {
    maxWidth: 120,
  },
  appContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& h4': {
      color: '#f8f8ff',
    },
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: 'rgba(255,255,255 ,0.9)',
    padding: 30,
    borderRadius: 20,
    '& Button': {
      marginTop: 20,
    },
  },
});

export default useStyles;

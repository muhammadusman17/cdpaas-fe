import React from 'react';
import { Button, Icon, TextField } from '@mui/material';
import ExampleForm from 'Components/form';
import GiantButton from 'Components/ui/CustomButton';
import Navbar from '../Layout/Navbar';
import Todo from 'Components/Todo/Todo';
import useStyles from './App.styles';
import ReactLogoPNG from './react.png';
import ReactLogoSVG from './react.svg';
import Pipelines from '../../pages/Pipelines/Pipelines';

const App = () => {
  const classes = useStyles();
  return (
    <>
      <Navbar />
      <div className={classes.app}>
        <div className={classes.appContent}>
          <div>
            <img className={classes.logo} src={ReactLogoPNG} alt="Logo" />
            <img className={classes.logo} src={ReactLogoSVG} alt="Logo SVG" />
          </div>
          <Todo />
          <h4>
            Webpack5, SWC, React and TypeScript BoilerPlate!
            <Icon>star</Icon>
          </h4>
          <Pipelines />
          <GiantButton>Custom Button</GiantButton>
          <div className={classes.inputContainer}>
            <TextField id="standard-basic" label="Username" variant="outlined" />
            <Button variant="outlined">OK</Button>
          </div>
          <ExampleForm />
        </div>
      </div>
    </>
  );
};

export default App;

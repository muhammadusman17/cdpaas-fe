import React from 'react';
import { screen } from '@testing-library/react';
import App from './App';
import { render } from '../../utils/test-utils';

test('Header contains correct text', () => {
  render(<App />);
  const text = screen.getByText('Webpack5, SWC, React and TypeScript BoilerPlate!');
  expect(text).toBeInTheDocument();
});

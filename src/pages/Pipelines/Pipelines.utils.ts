import { DataType } from 'Components/Graph/Graph.types';

export const IconNodeGraphData: DataType = {
  nodes: [
    {
      id: 'id01',
      name: 'Data Source 1',
      level: 0,
      nodeImage: 'images/sql.svg',
    },
    {
      id: 'id02',
      name: 'Data Source 2',
      level: 0,
      nodeImage: 'images/spreadsheet.png',
    },
    {
      id: 'id2',
      name: 'Transformation1',
      level: 1,
      nodeImage: 'images/transformation.png',
    },
    {
      id: 'id3',
      name: 'Transformation2',
      level: 2,
      nodeImage: 'images/transformation.png',
    },
    {
      id: 'id4',
      name: 'Transformation3',
      level: 3,
      nodeImage: 'images/transformation.png',
    },
    {
      id: 'id41',
      name: 'Transformation3.1',
      level: 3,
      nodeImage: 'images/transformation.png',
    },
    {
      id: 'id5',
      name: 'Transformation4',
      level: 4,
      nodeImage: 'images/transformation.png',
    },
    {
      id: 'id6',
      name: 'Destination',
      level: 5,
      nodeImage: 'images/aws.svg',
    },
  ],
  links: [
    {
      source: 'id01',
      target: 'id2',
    },
    {
      source: 'id02',
      target: 'id2',
    },
    {
      source: 'id2',
      target: 'id3',
    },
    {
      source: 'id2',
      target: 'id3',
    },
    {
      source: 'id3',
      target: 'id4',
    },
    {
      source: 'id3',
      target: 'id41',
    },
    {
      source: 'id41',
      target: 'id5',
    },
    {
      source: 'id4',
      target: 'id5',
    },
    {
      source: 'id5',
      target: 'id6',
    },
  ],
};

export const rectNodeGraphData: DataType = {
  nodes: [
    {
      id: 'id01',
      name: 'Data Source 1',
      level: 0,
    },
    {
      id: 'id02',
      name: 'Data Source 2',
      level: 0,
    },
    {
      id: 'id03',
      name: 'Data Source 3',
      level: 0,
    },
    {
      id: 'id04',
      name: 'Data Source 4',
      level: 0,
    },
    {
      id: 'id05',
      name: 'Data Source 5',
      level: 0,
    },
    {
      id: 'id06',
      name: 'Data Source 6',
      level: 0,
    },
    {
      id: 'id07',
      name: 'Data Source 7',
      level: 0,
    },
    {
      id: 'id08',
      name: 'Data Source 8',
      level: 0,
    },
    {
      id: 'id09',
      name: 'Data Source 9',
      level: 0,
    },
    {
      id: 'id010',
      name: 'Data Source 10',
      level: 0,
    },
    {
      id: 'id2',
      name: 'Transformation1',
      level: 1,
    },
    {
      id: 'id3',
      name: 'Transformation2',
      level: 2,
    },
    {
      id: 'id4',
      name: 'Transformation3',
      level: 3,
    },
    {
      id: 'id41',
      name: 'Transformation3.1',
      level: 3,
    },
    {
      id: 'id5',
      name: 'Transformation4',
      level: 4,
    },
    {
      id: 'id6',
      name: 'Destination',
      level: 5,
    },
  ],
  links: [
    {
      source: 'id01',
      target: 'id2',
    },
    {
      source: 'id02',
      target: 'id2',
    },
    {
      source: 'id03',
      target: 'id2',
    },
    {
      source: 'id04',
      target: 'id2',
    },
    {
      source: 'id05',
      target: 'id2',
    },
    {
      source: 'id06',
      target: 'id2',
    },
    {
      source: 'id07',
      target: 'id2',
    },
    {
      source: 'id08',
      target: 'id2',
    },
    {
      source: 'id09',
      target: 'id2',
    },
    {
      source: 'id010',
      target: 'id2',
    },
    {
      source: 'id2',
      target: 'id3',
    },
    {
      source: 'id2',
      target: 'id3',
    },
    {
      source: 'id3',
      target: 'id4',
    },
    {
      source: 'id3',
      target: 'id41',
    },
    {
      source: 'id41',
      target: 'id5',
    },
    {
      source: 'id4',
      target: 'id5',
    },
    {
      source: 'id5',
      target: 'id6',
    },
  ],
};

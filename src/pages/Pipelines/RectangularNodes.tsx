import Graph from '../../components/Graph/Graph';
import React from 'react';
import { rectNodeGraphData } from './Pipelines.utils';
const RectangularNodes = () => {
  return <Graph graphData={rectNodeGraphData} linkType="blocked" nodeType="rectangle" />;
};

export default RectangularNodes;

import { Button, Modal } from '@mui/material';
import React, { useState } from 'react';
import IconNodes from './IconNodes';
import RectangularNodes from './RectangularNodes';

const Pipelines = () => {
  const [pipelineType, setPipeline] = useState<'one' | 'two'>('one');
  const [openModal, setOpenModal] = useState(false);
  return (
    <>
      <Modal open={openModal}>
        <div style={{ background: 'white' }}>
          <Button
            onClick={() =>
              setPipeline((prevValue) => {
                if (prevValue === 'one') return 'two';
                return 'one';
              })
            }
          >
            Toggle Pipeline
          </Button>
          <Button onClick={() => setOpenModal(false)}>Close Pipeline Modal</Button>
          {pipelineType === 'one' ? <RectangularNodes /> : <IconNodes />}
        </div>
      </Modal>
      <Button onClick={() => setOpenModal(true)}>Dumy Pipelines</Button>
    </>
  );
};

export default Pipelines;

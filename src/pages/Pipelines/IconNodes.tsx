import Graph from 'Components/Graph/Graph';
import React from 'react';
import { IconNodeGraphData } from './Pipelines.utils';

const IconNodes = () => {
  return <Graph graphData={IconNodeGraphData} nodeType="image" linkType="blocked" />;
};

export default IconNodes;

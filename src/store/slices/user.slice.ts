import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface UserState {
  authenticated: boolean;
  userData: { userName: string } | null;
}

const initialState: UserState = {
  authenticated: false,
  userData: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setAuthenticated: (state, action: PayloadAction<{ userName: string }>) => {
      const { userName } = action.payload;
      state.authenticated = true;
      state.userData = {
        userName,
      };
    },
    logout: (state) => {
      state.authenticated = false;
      state.userData = null;
    },
  },
});

export const { setAuthenticated, logout } = userSlice.actions;

export default userSlice.reducer;
